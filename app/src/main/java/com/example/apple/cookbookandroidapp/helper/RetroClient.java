package com.example.apple.cookbookandroidapp.helper;

import com.example.apple.cookbookandroidapp.api.ApiService;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetroClient {
    private static final String ROOT_URL = "http://172.20.10.4:8080/";
    private static Retrofit getRetrofitInstance() {
        return new Retrofit.Builder()
                .baseUrl(ROOT_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static ApiService getApiService() {

        return getRetrofitInstance().create(ApiService.class);
    }
}
