package com.example.apple.cookbookandroidapp;

import android.annotation.SuppressLint;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.apple.cookbookandroidapp.adapter.MyAdapter;
import com.example.apple.cookbookandroidapp.adapter.ViewPagerAdapter;
import com.example.apple.cookbookandroidapp.api.ApiService;
import com.example.apple.cookbookandroidapp.helper.RetroClient;
import com.example.apple.cookbookandroidapp.model.DetailField;
import com.example.apple.cookbookandroidapp.model.DetailFieldList;
import com.example.apple.cookbookandroidapp.model.MyField;
import com.example.apple.cookbookandroidapp.model.MyFieldList;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DetailsActivity extends AppCompatActivity {
    TextView method_txt;
    private ArrayList<DetailField> detailFieldList;



    final ApiService api = RetroClient.getApiService();



    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        method_txt = findViewById(R.id.method);
        Call<DetailFieldList> call = api.getSelectAppDetails();
        call.enqueue(new Callback<DetailFieldList>() {
            @Override
            public void onResponse(Call<DetailFieldList> call, Response<DetailFieldList> response) {


                if (response.isSuccessful()) {
                    /**
                     * Got Successfully
                     */

                    detailFieldList = (ArrayList<DetailField>) response.body().getDetailField();

                }
                ArrayList<String> imageUrl = new ArrayList<>();


                Bundle extras = getIntent().getExtras();
                int key = extras.getInt("position");
                int position = key + 1;

                for( int i = 0;i<detailFieldList.size()-1;i++) {
                    int id = Integer.parseInt(detailFieldList.get(i).getId());

                    if (id==position) {

                        imageUrl.add(detailFieldList.get(i).getImageAddress());
                        System.out.println("@@@@@@@@@@@"+imageUrl);

                        method_txt.setText(detailFieldList.get(i).getMethod());
                    }
                }




                    System.out.println("@@@@@@@@@@@"+imageUrl);






                        ViewPager viewPager = findViewById(R.id.mViewPager);

                        ViewPagerAdapter adapter = new ViewPagerAdapter(getBaseContext(), imageUrl);
                        viewPager.setAdapter(adapter);
                        CircleIndicator indicator = (CircleIndicator) findViewById(R.id.indicator);
                        indicator.setViewPager(viewPager);
                    }







            @Override
            public void onFailure(Call<DetailFieldList> call, Throwable t) {
                Toast.makeText(getBaseContext(), "not connected to Details ", Toast.LENGTH_LONG).show();


            }

        });



    }
}
