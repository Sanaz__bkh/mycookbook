package com.example.apple.cookbookandroidapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class MyFieldList {

    @SerializedName("myField")
    @Expose
    private List<MyField> myField = null;

    public List<MyField> getMyField() {
        return myField;
    }

    public void setMyField(List<MyField> myField) {
        this.myField = myField;
    }
}
