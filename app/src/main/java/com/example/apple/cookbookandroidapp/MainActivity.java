package com.example.apple.cookbookandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {


     ImageButton appetizer_bn;
    ImageButton main_bn;

    ImageButton dessert_bn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        appetizer_bn = findViewById(R.id.appetizerButton);
        main_bn = findViewById(R.id.mainButton);
        dessert_bn = findViewById(R.id.dessertButton);

        appetizer_bn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                Intent intent1 = intent.putExtra("type","appetizers");

                startActivity(intent);
            }
        });

        main_bn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                Intent intent1 = intent.putExtra("type","main");
                startActivity(intent1);

            }
        });

        dessert_bn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, ListActivity.class);
                Intent intent1 = intent.putExtra("type","desserts");
                startActivity(intent);

            }
        });

    }
}
