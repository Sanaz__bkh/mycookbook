package com.example.apple.cookbookandroidapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.apple.cookbookandroidapp.adapter.MyAdapter;
import com.example.apple.cookbookandroidapp.api.ApiService;
import com.example.apple.cookbookandroidapp.helper.RetroClient;
import com.example.apple.cookbookandroidapp.model.MyField;
import com.example.apple.cookbookandroidapp.model.MyFieldList;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecipeListActivity extends AppCompatActivity implements MyAdapter.OnClickListener {
    private static final String TAG = "Details Activity";
    private ArrayList<MyField> myFieldList;
    private RecyclerView recyclerView;
    private MyAdapter eAdapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recipelist);
        final ApiService api = RetroClient.getApiService();
//
//        /**
//         * Calling JSON
//         */
        Bundle extras = getIntent().getExtras();
        String key = extras.getString("type");

        if (key.equals("appetizers")) {
            Call<MyFieldList> call = api.getSelectAppetizers();
            Toast.makeText(getBaseContext(), "step 1 ", Toast.LENGTH_LONG).show();

            call.enqueue(new Callback<MyFieldList>() {

                @Override
                public void onResponse(Call<MyFieldList> call, Response<MyFieldList> response) {
                    Toast.makeText(getBaseContext(), "step 2 ", Toast.LENGTH_LONG).show();


                    if (response.isSuccessful()) {
                        Toast.makeText(getBaseContext(), "step 3 ", Toast.LENGTH_LONG).show();

                        /**
                         * Got Successfully
                         */
                        myFieldList = (ArrayList<MyField>) response.body().getMyField();
                        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                        eAdapter = new MyAdapter(myFieldList);
                        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(eLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(eAdapter);
                        eAdapter.setOnClickListener(RecipeListActivity.this);


                    }

                }

                @Override
                public void onFailure(Call<MyFieldList> call, Throwable t) {
                    Toast.makeText(getBaseContext(), "not connected to server ", Toast.LENGTH_LONG).show();



                }


            });

        } else if (key.equals("main")) {
            Call<MyFieldList> call = api.getSelectMain();
            call.enqueue(new Callback<MyFieldList>() {
                @Override
                public void onResponse(Call<MyFieldList> call, Response<MyFieldList> response) {


                    if (response.isSuccessful()) {
                        /**
                         * Got Successfully
                         */
                        myFieldList = (ArrayList<MyField>) response.body().getMyField();
                        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                        eAdapter = new MyAdapter(myFieldList);
                        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(eLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(eAdapter);
                        eAdapter.setOnClickListener(RecipeListActivity.this);


                    }

                }


                @Override
                public void onFailure(Call<MyFieldList> call, Throwable t) {
                    Toast.makeText(getBaseContext(), "not connected to server ", Toast.LENGTH_LONG).show();


                }

            });


        } else if (key.equals("desserts")) {

            Call<MyFieldList> call = api.getSelectDesserts();
            call.enqueue(new Callback<MyFieldList>() {
                @Override
                public void onResponse(Call<MyFieldList> call, Response<MyFieldList> response) {


                    if (response.isSuccessful()) {
                        /**
                         * Got Successfully
                         */
                        myFieldList = (ArrayList<MyField>) response.body().getMyField();
                        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
                        eAdapter = new MyAdapter(myFieldList);
                        RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
                        recyclerView.setLayoutManager(eLayoutManager);
                        recyclerView.setItemAnimator(new DefaultItemAnimator());
                        recyclerView.setAdapter(eAdapter);
                        eAdapter.setOnClickListener(RecipeListActivity.this);


                    }

                }

                @Override
                public void onFailure(Call<MyFieldList> call, Throwable t) {
                    Toast.makeText(getBaseContext(), "not connected to server ", Toast.LENGTH_LONG).show();



                }


            });

        }


        //Call<MyFieldList> call = api.getSelectAppetizers();


        /**
         * Enqueue Callback will be call when get response...
         */
//        call.enqueue(new Callback<MyFieldList>() {
//            @Override
//            public void onResponse(Call<MyFieldList> call, Response<MyFieldList> response) {
//
//
//                if (response.isSuccessful()) {
//                    /**
//                     * Got Successfully
//                     */
//                    myFieldList = (ArrayList<MyField>) response.body().getMyField();
//                    recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
//                    eAdapter = new MyAdapter(myFieldList);
//                    RecyclerView.LayoutManager eLayoutManager = new LinearLayoutManager(getApplicationContext());
//                    recyclerView.setLayoutManager(eLayoutManager);
//                    recyclerView.setItemAnimator(new DefaultItemAnimator());
//                    recyclerView.setAdapter(eAdapter);
//                }
//
//            }
//
//            @Override
//            public void onFailure(Call<MyFieldList> call, Throwable t) {
//                Toast.makeText(getBaseContext(),"not connected to server ",Toast.LENGTH_LONG).show();
//
//
//            }
//
//
//        });


    }


    @Override
    public void onItemClick(int position) {
        Intent intent = new Intent(this, DetailsActivity.class);
        Toast.makeText(getBaseContext(), "position is " + position, Toast.LENGTH_LONG).show();
        intent.putExtra("position",position);
        startActivity(intent);


    }
}