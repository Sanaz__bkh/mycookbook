package com.example.apple.cookbookandroidapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DetailFieldList {
    @SerializedName("detailField")
    @Expose
    private List<DetailField> detailField = null;

    public List<DetailField> getDetailField() {
        return detailField;
    }

    public void setDetailField(List<DetailField> detailField) {
        this.detailField = detailField;
    }
}
