package com.example.apple.cookbookandroidapp.api;

import com.example.apple.cookbookandroidapp.model.DetailField;
import com.example.apple.cookbookandroidapp.model.DetailFieldList;
import com.example.apple.cookbookandroidapp.model.MyFieldList;

import retrofit2.Call;
import retrofit2.http.GET;

    public interface ApiService {
        @GET("ws/service/appetizers")
        Call<MyFieldList> getSelectAppetizers();

        @GET("ws/service/main")
        Call<MyFieldList> getSelectMain();

        @GET("ws/service/desserts")
        Call<MyFieldList> getSelectDesserts();

        @GET("ws/service/appDetails")
        Call<DetailFieldList> getSelectAppDetails();
    }

