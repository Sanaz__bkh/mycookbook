package com.example.apple.cookbookandroidapp.adapter;


import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.apple.cookbookandroidapp.model.DetailFieldList;
import com.example.apple.cookbookandroidapp.model.MyField;
import com.example.apple.cookbookandroidapp.R;
import com.example.apple.cookbookandroidapp.model.MyFieldList;
import com.squareup.picasso.Picasso;

import java.util.List;

import retrofit2.Call;


public class MyAdapter extends RecyclerView.Adapter<MyAdapter.Holder> {
    private List<MyField> myFieldsList;
    private OnClickListener mlistener;


    public interface OnClickListener {
        void onItemClick(int position);

    }
    public void setOnClickListener(OnClickListener listener ){
        this.mlistener = listener;


    }

    public MyAdapter(List<MyField> myFieldsList) {
        this.myFieldsList = myFieldsList;
    }




    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.eachcard, viewGroup, false);

        Holder holder = new Holder(view);


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final Holder holder, int i) {
        holder.binder(myFieldsList.get(i));

    }

    @Override
    public int getItemCount() {
        return myFieldsList.size();
    }
    public interface CallbackAction{
        void gotoItemDetails(MyField myField);
    }

    public class Holder extends RecyclerView.ViewHolder  {
        private ImageView image_txt;
        private TextView name_txt;
        private View itemView;

        public Holder(@NonNull View itemView) {
            super(itemView);
            this.itemView = itemView;

            image_txt = itemView.findViewById(R.id.image);
            name_txt = itemView.findViewById(R.id.name);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(mlistener != null){
                        int position = getAdapterPosition();
                        if(position != RecyclerView.NO_POSITION){ // check position avaliblity
                            mlistener.onItemClick(position);
                        }

                    }
                }
            });


        }

        public void binder(MyField item) {
            Picasso.get().load(item.getImageAddress()).fit().centerCrop().into(image_txt);
            name_txt.setText(item.getName());


        }
        }



    }



